#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QTextStream>
#include <QString>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_openFile_clicked()
{
    QString source = QFileDialog::getOpenFileName(this, "Выберите файл для чтения", "", "Текстовый файл *.txt");
    QFile file(source);
    if(file.open(QFile::ReadOnly)) // если файл открылся
    {
        QTextStream inputText(&file);
        while(!inputText.atEnd())
        {
            strSave.append(inputText.readLine());
            strSave.append("!\n");
        }
        ui->textWind->setText(strSave);
    }
}

void MainWindow::on_saveFile_clicked()
{

    QString destination = QFileDialog::getOpenFileName(this, "Выберите файл для записи", "", "Текстовый файл *.txt");
    QFile file(destination);

    if(file.open(QFile::WriteOnly | QFile::Truncate)) // если файл открылся
    {
        QTextStream out(&file);
        //out<< ui->textWind;
        out<< strSave;
    }
}

