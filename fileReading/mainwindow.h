#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    QString strSave;

private:
    Ui::MainWindow *ui;

private slots:
    void on_openFile_clicked();
    void on_saveFile_clicked();
};

#endif // MAINWINDOW_H
